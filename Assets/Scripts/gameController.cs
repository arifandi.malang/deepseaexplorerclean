﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;

using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

using UnityEngine.Networking;

//using UnityEngine.Advertisements;

public class gameController : MonoBehaviour
{

    private Text signInButtonText;
    private Text authStatus;
    private Text idStatus;

    private Text savedState;

    private int coin;

    public string myGameIDAndroid = "";

    public bool adStarted;
    public bool adCompleted;
    private bool testMode = true;
    private string gid = "default";

    private string urlAPI = "api.distudiomalang.com";

    //ShowOptions options = new ShowOptions();

    // Start is called before the first frame update
    void Start()
    {
        GameObject startButton = GameObject.Find("playButton");

        signInButtonText =
          GameObject.Find("signInButton").GetComponentInChildren<Text>();

        authStatus = GameObject.Find("authStatus").GetComponent<Text>();
        idStatus = GameObject.Find("idStatus").GetComponent<Text>();

        savedState = GameObject.Find("savedState").GetComponent<Text>();

        savedState.text = coin.ToString();

        EventSystem.current.firstSelectedGameObject = startButton;
        
        //  ADD THIS CODE BETWEEN THESE COMMENTS

        // Create client configuration
            PlayGamesClientConfiguration config = new
            PlayGamesClientConfiguration.Builder()
            .EnableSavedGames()
            .Build();

        // Enable debugging output (recommended)
        PlayGamesPlatform.DebugLogEnabled = true;

        // Initialize and activate the platform
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
        // END THE CODE TO PASTE INTO START

        PlayGamesPlatform.Instance.Authenticate(SignInCallback, true);


    }

    public void SignIn()
    {
        Debug.Log("signInButton clicked!");

        if (!PlayGamesPlatform.Instance.localUser.authenticated)
        {
            // Sign in with Play Game Services, showing the consent dialog
            // by setting the second parameter to isSilent=false.
            PlayGamesPlatform.Instance.Authenticate(SignInCallback, false);
        }
        else
        {
            // Sign out of play games
            PlayGamesPlatform.Instance.SignOut();

            // Reset UI
            signInButtonText.text = "Sign In";
            authStatus.text = "";
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        //writeMethod();
    }
    


    public void SignInCallback(bool success)
    {
        if (success)
        {
            Debug.Log("(Lollygagger) Signed in!");

            // Change sign-in button text
            signInButtonText.text = "Sign out";
            
            // Show the user's name
            authStatus.text = "Signed in as: " + Social.localUser.userName;
            idStatus.text = "Player ID : " + Social.localUser.id;
            gid = Social.localUser.id;
        }
        else
        {
            Debug.Log("(Lollygagger) Sign-in failed...");

            // Show failure message
            signInButtonText.text = "Sign in";
            authStatus.text = "Sign-in failed";
        }
    }

    public void register_user_method() {
        StartCoroutine(register_user_numerator());
    }


    IEnumerator register_user_numerator()
    {
       //[[gid = "jhkjh";
        //List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
        //formData.Add(new MultipartFormDataSection("gid='q7hjg'"));
        //formData.Add(new MultipartFormDataSection("token=" + gid));

        WWWForm form = new WWWForm();
        form.AddField("gid", gid);

        UnityWebRequest www = UnityWebRequest.Post(urlAPI+"/register_user", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Form upload complete!");
        }
    }



}
