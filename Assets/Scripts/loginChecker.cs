﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BayatGames.SaveGameFree;


public class loginChecker : MonoBehaviour
{

    public int gold;
    public int userid;

    GameObject textGold;
    GameObject textUserid;

    void Start()
    {

        // Saving the data
        

        textGold = GameObject.Find("textGold");
        textUserid = GameObject.Find("textUserid");

        try {
            userid = SaveGame.Load<int>("userid");
            gold = SaveGame.Load<int>("gold");
        }
        catch(System.Exception e){
            Debug.Log("user iderror");
        }
        

        if (userid == 0)
        {
            
            textUserid.GetComponent<Text>().text = "null";
            userid = Random.Range(0, 10000);
            SaveGame.Save<int>("userid", userid);
            Debug.Log("randoming user id : " + userid);
        }
        else {
            textUserid.GetComponent<Text>().text = userid.ToString();
        }
    }

    private void Update()
    {
        textGold.GetComponent<Text>().text = gold.ToString();
    }


    public void saveGold() {
        SaveGame.Save<int>("gold", gold);
    }

    public void loadGold() {
        gold = SaveGame.Load<int>("gold");
    }

    public void addGold() {
        gold += 1;
    }

    public void loadNewUser() {
        Debug.Log("loaded new user id : "+ userid.ToString());
        textUserid.GetComponent<Text>().text = userid.ToString();
    }

   

}
